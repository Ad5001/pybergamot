#!/usr/bin/env bash
python3 -m pdoc --force --html pybergamot

cd html/pybergamot || exit

python3 -m http.server 8080
