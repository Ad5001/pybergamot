# pybergamot

(Somewhat) stable interface for the **Bergamot Translation Engine Python Bindings**.

## Generate documentation

Documentation for `pybergamot` can be generated using [pdoc](https://pdoc.dev).

Use the script `build-doc.sh` to generate the documentation directly.

## Legal

        pybergamot - (Somewhat) stable interface for the **Bergamot Translation Engine Python Bindings**.
        Copyright (C) 2023 Ad5001 <mail@ad5001.eu>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

This project is not affiliated to the Bergamot Project or Mozilla.